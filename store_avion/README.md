1.	Посилання на задеплоїний сайт – https://shop-avion.onrender.com/

2. Що зроблено:
• Налаштування prettier, eslint та react-router. 
• Реалізація header, сторінки авторизації користувача, форми регістрації та входу, форми для замовлення товарів з корзини, замовлення товарів та сторінка замовлень користувача.
• Налаштування Redux. 
• Реалізація компоненту ProductsContainer на головній сторінці, компоненту Search – пошуку товарів, сторінки одного продукту (ProductsPage), корзини та сторінки Сontact Us з підключенням  react-google-maps/api.
• Реалізація footer, сторінки каталогу товарів, фільтрації товарів, сторінки товарів по окремим категоріям, стильове оформлення контактної інформації на сторінці Сontact Us. 
• Налаштування бази даних, створення відповідних колекцій, редагування позицій в колекціях бази даних.
• Реалізація компоненту advantages (What makes our brand different) на головній сторінці, тосту, що з’являється при надсиланні даних на сервер, модального вікна, сторінки About Us. 
• Реалізація компонентів MainImage та MainVideo на головній сторінці, сторінок NotFountPage та PravicyPage. 
• Розроблення компоненту It Started на головній сторінці, додавання до wishlist товарів, сторінки wishlist. 

3.	Які технології були використані:
•	Основні інструменти: React, React Router, Redux, CSS Modules, Axios, ESLint, Prettier;
•	Додаткові інструменти: , Node.js, Express JS, Mongo DB;
•	Бібліотеки: Formik, Yup, react-google-maps/api, react-input-mask, react-icons, react-spring, jwt- decode
•	Тестування: Jest, React Testing Library, axios-mock-adapter, redux-mock-store;

4)	Тестування
•	написання тестів


