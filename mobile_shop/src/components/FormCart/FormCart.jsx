import React from "react";
import { Form, Formik } from "formik";
import * as Yup from "yup";
import { useDispatch, useSelector } from "react-redux";
import { cartCheckout } from "../../Redux/cart/action-creators";
import { clearCart } from "../../Redux/product/action-creators";
import { TextAreaInput } from "../TextAreaInput/TextAreaInput";
import { TextInput } from "../TextInput/TextInput";

export const FormCart = () => {
  const dispatch = useDispatch();
  const cart = useSelector((state) => state.product.product).filter(
    (e) => e.cart.inCart === "true"
  );

  return (
    <>
      <h4>Order form</h4>
      <Formik
        initialValues={{
          firstName: "",
          lastName: "",
          age: "",
          email: "",
          phone: "",
          address: "",
        }}
        validationSchema={validationFormsSchema}
        onSubmit={(values, { setSubmitting }) => {
          setSubmitting(false);
          dispatch(clearCart());
          dispatch(cartCheckout({ customer: values, product: cart }));
        }}
      >
        <Form>
          <div className="row">
            <div className={"col s4"}>
              <TextInput label="First Name" name="firstName" type="text" />
            </div>
            <div className={"col s4"}>
              <TextInput label="Last name" name="lastName" type="text" />
            </div>
            <div className={"col s4"}>
              <TextInput label="Age" name="age" type="number" />
            </div>
          </div>

          <div className="row">
            <div className={"col s6"}>
              <TextInput label="Email Address" name="email" type="email" />
            </div>
            <div className={"col s6"}>
              <TextInput label="Telephone" name="phone" type="tel" />
            </div>
          </div>
          <div className="row">
            <div className={"col s12"}>
              <TextAreaInput label="Delivery address" name="address" />
            </div>
          </div>

          <div className="row center-align">
            <button
              className="btn waves-effect waves-light orange accent-3 btn-large "
              type="submit"
              name="action"
            >
              Checkout
              <i className="material-icons right">shopping_cart</i>
            </button>
          </div>
        </Form>
      </Formik>
    </>
  );
};




const phoneRegExp =
  /[+3]|^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/;
// const textRegExp = /\s\D/;
const validationFormsSchema = Yup.object().shape({
  firstName: Yup.string()
    // .matches(textRegExp, "The field must not contain numbers")
    .typeError("Should be a string")
    .max(20, "Must be 20 characters or less")
    .required("Required"),
  lastName: Yup.string()
    // .matches(textRegExp, "The field must not contain numbers")
    .typeError("Should be a string")
    .max(20, "Must be 20 characters or less")
    .required("Required"),
  age: Yup.number()
    .typeError("Should be a number")
    .required("Required")
    .max(90, "some error, maximum age 90")
    .min(18, "some error, minimum age 18"),
  email: Yup.string().email("Invalid email address").required("Required"),
  phone: Yup.string()
    .required("required")
    .matches(phoneRegExp, "Phone number is not valid")
    .min(13, "to short")
    .max(13, "to long"),
  address: Yup.string()
    .typeError("Should be a string")
    .min(8, "Must be 8 characters or less")
    .max(120, "Must be 120 characters or less")
    .required("Required"),
});
