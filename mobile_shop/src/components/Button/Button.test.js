import { Button } from "./Button";
import { render } from "@testing-library/react";
import userEvent from "@testing-library/user-event";

describe("Test for button", () => {
  test("Smoke component footer", () => {
    render(<Button />);
  });
  test("Show text for button", () => {
    const textTest = "textTest";
    const { getByText } = render(<Button text={textTest} />);
    getByText(textTest);
  });
  test("Show icon for button", () => {
    const icon = '<i className="material-icons left">shopping_cart</i>';
    const { getByText } = render(<Button text={icon} />);
    getByText(icon);
  });
  test("Show color for button", () => {
    const color = "red";
    const { getByTestId } = render(<Button color={color} />);
    expect(getByTestId("button")).toHaveClass(color);
  });
  test("Test working function onClick ", () => {
    const onClickFn = jest.fn();
    const { getByTestId } = render(<Button click={onClickFn} />);
    userEvent.click(getByTestId("button"));
    expect(onClickFn).toHaveBeenCalled();
  });
  test("Snapshot test for button", () => {
    const { container } = render(<Button />);
    expect(container.innerHTML).toMatchSnapshot();
  });
});
