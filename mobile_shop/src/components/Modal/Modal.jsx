import React from "react";
import styles from "./Modal.module.scss";
import PropTypes from "prop-types";
import { Button } from "../Button/Button";

export const Modal = ({ isOpen, header, closeButton, textModal, actions }) => {
  return (
    <div data-testid="modal" onClick={isOpen} className={styles.modal}>
      <div
        id={"modalBody"}
        data-testid="modalBody"
        onClick={(event) => event.stopPropagation()}
        className={styles.modal__content}
      >
        <div data-testid="modalHeader" className={styles.modal__header}>
          <h5 className={styles.modal__title}>{header}</h5>
          {closeButton && (
            <span data-testid="modalCloseButton" onClick={isOpen}>
              &#10761;
            </span>
          )}
        </div>
        <div className={styles.modal__body}>
          <p className={styles.body__text}>{textModal}</p>
        </div>
        {actions}
      </div>
    </div>
  );
};
export default Modal;

Button.propTypes = {
  isOpen: PropTypes.func,
  header: PropTypes.string,
  textModal: PropTypes.string,
  closeButton: PropTypes.bool,
  actions: PropTypes.object,
};
Button.defaultProps = {
  closeButton: true,
};
