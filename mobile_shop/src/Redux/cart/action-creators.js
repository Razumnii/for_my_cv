import { CHECKOUT } from "./actions";

export const cartCheckout = (payload) => ({ type: CHECKOUT, payload });
