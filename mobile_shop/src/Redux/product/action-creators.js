import * as types from "./actions";
export const isLoading = () => ({ type: types.LOADING_CARDS });
export const isError = (payload) => ({ type: types.LOADING_ERROR, payload });
export const fetchCarts = (payload) => ({
  type: types.FETCH_CARTS_SUCCESS,
  payload,
});
export const favorite = (payload) => ({ type: types.TOGGLE_FAVORITE, payload });
export const addInCart = (payload) => ({ type: types.ADD_TO_CART, payload });
export const removeOneCart = (payload) => ({
  type: types.REMOVE_ONE_CART,
  payload,
});
export const removeCart = (payload) => ({
  type: types.REMOVE_PRODUCT_CART,
  payload,
});
export const clearCart = (payload) => ({ type: types.CLEAR_CART, payload });
