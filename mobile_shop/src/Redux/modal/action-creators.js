import * as types from "./actions";

export const toggleShowModal = () => ({ type: types.MODAL_SHOW });
export const idModalProduct = (payload) => ({
  type: types.MODAL_ID_PRODUCT,
  payload,
});
