import getAllCards from "../API/getAllCards.js";
import { LOGIN_BUTTON, LOGOUT_BUTTON, CREATE_CARD_BUTTON, MAIN_CONTENT_CONTAINER, NO_ITEMS_WRAPPER, CARDS_CONTAINER } from "../constants/constants.js";
import Card from "../classes/Card.js";

const showUserAccount = async () => {
	document.querySelector(LOGIN_BUTTON).style.display = 'none';
	document.querySelector(CREATE_CARD_BUTTON).style.display = 'block';
	document.querySelector(LOGOUT_BUTTON).style.display = 'block';
	document.querySelector(MAIN_CONTENT_CONTAINER).style.display = 'block';

	const cards = await getAllCards();

	if(cards.length === 0) {
		document.querySelector(NO_ITEMS_WRAPPER).style.display = 'block';
	} else {
		cards.sort((a, b) => a.id - b.id)
			.forEach(card => new Card(card).render(CARDS_CONTAINER));
	}
}

export default showUserAccount;