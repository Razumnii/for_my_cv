export const LOCAL_STORAGE_TOKEN = 'token';

export const LOGIN_BUTTON = '#login-btn';
export const LOGOUT_BUTTON = '#logout-btn';
export const CREATE_CARD_BUTTON = '#create-card-btn';
export const MAIN_CONTENT_CONTAINER = '#main-content';
export const NO_ITEMS_WRAPPER = '#no-items-wrapper';
export const CARDS_CONTAINER = '#cards-container';


