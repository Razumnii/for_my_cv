import instance from "./instance.js";

const getAllCards = async () => {
  try {
    const { data: cards } = await instance.get("/");
    // console.log(cards);
    return cards;
  } catch (error) {
    console.log(error);
  }
};

export default getAllCards;
