/*OUR SERVICE */
const tabs = document.querySelectorAll(".tab");
const tabContents = document.querySelectorAll(".tab-content");

tabs.forEach((tab) => {
  tab.addEventListener("click", () => {
    const target = tab.dataset.target;
    tabContents.forEach((content) => {
      if (content.id === target) {
        content.classList.add("active");
      } else {
        content.classList.remove("active");
      }
    });
    tabs.forEach((tab) => {
      if (tab.dataset.target === target) {
        tab.classList.add("selected");
      } else {
        tab.classList.remove("selected");
      }
    });
  });
});


/*OUR TEAM */
const tabs_second = document.querySelectorAll('.workBlock_tab');
const tabContents_second = document.querySelectorAll('.workBlock_tab-content');

tabs_second.forEach(tab => {
  tab.addEventListener('click', () => {
    const tabId = tab.getAttribute('data-tab');
    tabs_second.forEach(tab => tab.classList.remove('active'));
    tab.classList.add('active');
    tabContents_second.forEach(content => {
      if (content.getAttribute('data-tab') === tabId) {
        content.classList.add('active');
        const loadMoreBtn = content.querySelector('.workBlock_load-more');
        if (loadMoreBtn) {
          loadMoreBtn.addEventListener('click', () => {
            let hiddenImages = content.querySelectorAll('.workBlock_image.hidden');
            let imagesToShow = hiddenImages.length > 4 ? 4 : hiddenImages.length;

            for (let i = 0; i < imagesToShow; i++) {
              hiddenImages[i].classList.remove('hidden');
            }

            if (content.querySelectorAll('.workBlock_image.hidden').length === 0) {
              loadMoreBtn.style.display = 'none';
            }
          });
        }
      } else {
        content.classList.remove('active');
      }
    });
  });
});

/*LOAD MORE */

let activeTab = document.querySelector('.workBlock_tab-content');
let images = activeTab.querySelectorAll('.workBlock_image.hidden');
let loadMoreBtn = activeTab.querySelector('.workBlock_load-more');
let imagesToLoad = 4;

loadMoreBtn.addEventListener('click', () => {
  let hiddenImages = activeTab.querySelectorAll('.workBlock_image.hidden');
  let imagesToShow = hiddenImages.length > imagesToLoad ? imagesToLoad : hiddenImages.length;

  for (let i = 0; i < imagesToShow; i++) {
    hiddenImages[i].classList.remove('hidden');
  }

  if (activeTab.querySelectorAll('.workBlock_image.hidden').length === 0) {
    loadMoreBtn.style.display = 'none';
  }
});

/*SAY */
const sliderItems = document.querySelectorAll('.peopleSay_wrapper');
const sliderImages = document.querySelectorAll('.image-carousel__wrapper img');
const prevButton = document.querySelector('#prevButton');
const nextButton = document.querySelector('#nextButton');

let currentSlide = 0;

function hideSliderItems() {
  sliderItems.forEach(sliderItem => {
    sliderItem.classList.add('hide');
  });
  sliderItems[currentSlide].classList.remove('hide');
}

function prevSlide() {
  currentSlide--;
  if (currentSlide < 0) {
    currentSlide = sliderItems.length - 1;
  }
  hideSliderItems();
}

function nextSlide() {
  currentSlide++;
  if (currentSlide >= sliderItems.length) {
    currentSlide = 0;
  }
  hideSliderItems();
}

prevButton.addEventListener('click', prevSlide);
nextButton.addEventListener('click', nextSlide);

sliderImages.forEach((sliderImage, index) => {
  sliderImage.addEventListener('click', () => {
    currentSlide = index;
    hideSliderItems();
  });
});

hideSliderItems();











